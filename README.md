# 软件工程专业教育

- 软件工程2019-1-2班，星期六(11月14日)  8:30-17:30  10502
- 软件工程2019-3-4班，待定
- 软件工程2018-1-2班，待定
- 软件工程2018-3-4班，星期天(11月15日)  8:30-17:30  10502

## 时间安排

- 上午：
  - 专业教育
  - AWS云计算介绍
  - 老师项目介绍
- 下午：
  - 个人职业规划
  - 报告撰写

## AWS

- https://aws.amazon.com/cn/console/
- https://aws.amazon.com/cn/education/awseducate/
- www.awsacademy.com/sitelogin
- https://www.awsacademy.com/LMS_Login
